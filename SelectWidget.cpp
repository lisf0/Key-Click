#include "SelectWidget.h"
#include "ui_selectWidget.h"
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>

#include <qmessagebox.h>
#include "src/mainwindow.h"

SelectWidget::SelectWidget(QWidget *parent, QRect rect):
QWidget(parent),
  ui(new Ui::selectWidget),
  m_rect(rect)
{
   initUI();
}

SelectWidget::~SelectWidget()
{
    delete ui;
}

void SelectWidget::setSize(QRect rect)
{
	m_rect = rect;
	setGeometry(m_rect);
}

void SelectWidget::initUI()
{
   ui->setupUi(this);

   setMouseTracking(true);
   setWindowFlags(Qt::FramelessWindowHint);//无边框
   setWindowOpacity(0.2);
   setGeometry(m_rect);
    //setAttribute(Qt::WA_TranslucentBackground);//背景透明

}

void SelectWidget::mousePressEvent(QMouseEvent * event)
{
	QWidget::mousePressEvent(event);
}

void SelectWidget::mouseReleaseEvent(QMouseEvent * event)
{
	QWidget::mouseReleaseEvent(event);
}

void SelectWidget::mouseMoveEvent(QMouseEvent *event)
{
    m_mouseX=event->pos().x();
    m_mouseY=event->pos().y();
    //QString str2 = QString("Get the mouse position (%1,%2)").arg(x).arg(y);
    //statusBar()->showMessage(str2, 500);
    update();
}

void SelectWidget::mouseDoubleClickEvent(QMouseEvent * event)
{
	m_selectPoint = event->globalPos();
	QMessageBox::StandardButton rb = QMessageBox::information(this, "提示", QString("标 position %1,%2 ").arg(m_selectPoint.rx()).arg(m_selectPoint.ry()), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

	if (rb == QMessageBox::Yes)
	{
//        QWidget* widget = static_cast<QWidget*>(this->parent());
//        if(widget)
//        {
//            widget->show();
//            this->hide();
//        }
    }

}

void SelectWidget::closeEvent(QCloseEvent *event)
{
    //如果使用任务栏的关闭窗口,让其显示mainwindows
    MainWindow::getInstance()->show();
}

void SelectWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    QString str = QString("x:%1,y:%2").arg(m_mouseX).arg(m_mouseY);

    {   //跟随 圆 + 文字
//        painter.setPen(Qt::red);  //画笔颜色
//        QPoint point = QPoint(m_mouseX,m_mouseY);
//        //painter.setBrush(Qt::yellow);   //实心
//        //painter.drawEllipse(point,50,50);   //空心
//        painter.drawText(m_mouseX,m_mouseY,str);
    }

    {   //绘画固定位置 矩形 + 文字
//        QRectF ff(100,100,300,200);
//        //设置一个矩形
//        painter.drawRect(ff);
//        //为了更直观地看到字体的位置，我们绘制出这个矩形
//        painter.setPen(QColor(Qt::red));
//        //设置画笔颜色为红色
//        painter.drawText(ff,Qt::AlignHCenter,str);
    }
    {
        QFont font("Arial",20,QFont::Bold,true);//设置字体的类型，大小，加粗，斜体
        //font.setUnderline(true);//设置下划线
        //font.setOverline(true);//设置上划线
        //font.setCapitalization(QFont::SmallCaps);//设置大小写
        font.setLetterSpacing(QFont::AbsoluteSpacing,5);//设置间距
        painter.setFont(font);//添加字体

        painter.setPen(QColor(Qt::red));
        painter.drawText(m_mouseX,m_mouseY,str);
    }
}
