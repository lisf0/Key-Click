#ifndef SelectWidget_H
#define SelectWidget_H

#include <QWidget>

namespace Ui {
class selectWidget;
}

class SelectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SelectWidget(QWidget *parent = 0,QRect rect = QRect(0,0,400,300));
    ~SelectWidget();

	void setSize(QRect rect);
 private:
    void initUI();

	//QWidget 重写mouse事件
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent	*event);
	void mouseDoubleClickEvent(QMouseEvent *event);

    void closeEvent(QCloseEvent *event);

	//绘画事件
    void paintEvent(QPaintEvent *);
private:
    Ui::selectWidget *ui;

    QRect m_rect;
    int m_mouseX;
    int m_mouseY;

	QPoint m_selectPoint;
};

#endif // SelectWidget_H
