#include "listwidget.h"
#include "ui_listwidget.h"

ListWidget::ListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ListWidget),
    m_changeFun(nullptr),
    m_deleteFun(nullptr)
{
    ui->setupUi(this);

    connect(ui->btn_change, &QPushButton::clicked,this, &ListWidget::changeClick);
    connect(ui->btn_delete, &QPushButton::clicked,this, &ListWidget::deleteClick);
}

ListWidget::~ListWidget()
{
    delete ui;
}

void ListWidget::setText(const QString &text)
{
    if(ui&&ui->label)
    {
        ui->label->setText(text);
    }
}

void ListWidget::setChangeFun(std::function<void ()> fun)
{
    m_changeFun = fun;
}

void ListWidget::setDeleteFun(std::function<void ()> fun)
{
    m_deleteFun = fun;
}

void ListWidget::deleteClick()
{
    if(m_deleteFun)
    {
        m_deleteFun();
    }
}

void ListWidget::changeClick()
{
    if(m_changeFun)
    {
        m_changeFun();
    }
}
