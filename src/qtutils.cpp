#include "qtutils.h"
#include <QGuiApplication>
#include <qscreen.h>
#include <qcursor.h>
#include <qevent.h>
#include <qapplication.h>


QtUtils* QtUtils::m_instance = nullptr;

QtUtils::QtUtils(QObject *parent) : QObject(parent)
{

}

QtUtils *QtUtils::getInstance()
{
    if(m_instance==nullptr)
    {
		m_instance = new QtUtils();
    }

	return m_instance;
}

QRect QtUtils::getScreenRect()
{	
	QScreen *screen = QGuiApplication::primaryScreen();
	if (screen == nullptr)
	{
		return QRect();
	}

    return screen->availableGeometry();
}

QPoint QtUtils::getMousePoint()
{
	return QCursor::pos();
}

void QtUtils::moveMouse(QPoint point)
{
	QCursor::setPos(point);
}

void QtUtils::mouseClick(QPoint point, QObject *receiver)
{
	QMouseEvent event0(QEvent::MouseButtonPress, point, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QMouseEvent event1(QEvent::MouseButtonRelease, point, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);


	QApplication::sendEvent(receiver, &event0);
	QApplication::sendEvent(receiver, &event1);
}

//void QtUtils::mousePressEvent(QMouseEvent * event)
//{
//	int m_i_clickX = event->x();
//	int m_i_clickY = event->y();
//	//qDebug() << m_i_clickX << m_i_clickY;
//}
//
//void QtUtils::mouseReleaseEvent(QMouseEvent * event)
//{
//	int m_i_clickX = event->x();
//	int m_i_clickY = event->y();
//	//qDebug() << "release" << m_i_clickX << m_i_clickY;
//	if (event->button() == Qt::LeftButton)
//	{
//
//	}
//}
