#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class SelectWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    static MainWindow* getInstance();
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void mousePressEvent(QMouseEvent *event);       //自定义函数
    void mouseReleaseEvent(QMouseEvent *event);     //自定义函数
    void mouseMoveEvent(QMouseEvent * event);       //系统函数 添加mouseMover事件响应

    void addItem();
    void runActions();

private slots:
    void on_file_exit_triggered();

    void on_file_import_triggered();

    void on_file_export_triggered();

private:

private:
    Ui::MainWindow *ui;
	QStatusBar	*m_statusBar;
	QMenuBar	*m_menuBar;
	SelectWidget	*m_selectWidget;
	
};

#endif // MAINWINDOW_H
