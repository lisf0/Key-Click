#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qtutils.h"
#include <qlabel.h>
#include <qevent.h>
#include <qdatetime.h>
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qstandarditemmodel.h>
#include <QStandardItem>
#include <qpushbutton.h>
#include <qlistwidget.h>
#include <QDebug>

#include <QHBoxLayout>

#include "listwidget.h"
#include "SelectWidget.h"



MainWindow *MainWindow::getInstance()
{
    static MainWindow win;
    return &win;
}

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	m_selectWidget(nullptr)
{
	ui->setupUi(this);


    connect(ui->btn_addItem, &QPushButton::clicked,this, &MainWindow::addItem);
    connect(ui->btn_run, &QPushButton::clicked,this, &MainWindow::runActions);

	m_statusBar = statusBar();

	QRect rect = QtUtils::getInstance()->getScreenRect();

	QString str = QString("%1 x %2").arg(rect.width()).arg(rect.height());//  rect.width() + "*" + std::to_string(rect.height());
	QLabel *per1 = new QLabel(str, this);
	statusBar()->addPermanentWidget(per1);
	QPoint pint = QtUtils::getInstance()->getMousePoint();
    QString str2 = QString("get the mouse position (%1,%2)").arg(pint.x()).arg(pint.y());
	statusBar()->showMessage(str2, 3000);

    //this->centralWidget()->setMouseTracking(true);
    //setMouseTracking(true);

    //QtUtils::getInstance()->moveMouse(QPoint(1,1));
    //QtUtils::getInstance()->mouseClick(QPoint(400,1060),this);
//    for(int i=0;i<10;i++){
//       addItem();
//    }
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent * event)
{
	int m_i_clickX = event->x();
	int m_i_clickY = event->y();
	//qDebug() << m_i_clickX << m_i_clickY;
}

void MainWindow::mouseReleaseEvent(QMouseEvent * event)
{
	int m_i_clickX = event->x();
	int m_i_clickY = event->y();
	//qDebug() << "release" << m_i_clickX << m_i_clickY;
	if (event->button() == Qt::LeftButton)
	{

    }
}
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    int x=event->pos().x();
    int y=event->pos().y();
    QString str2 = QString("Get the mouse position (%1,%2)").arg(x).arg(y);
    statusBar()->showMessage(str2, 500);
    update();
}

void MainWindow::addItem()
{
    ListWidget *widget = new ListWidget();
    QListWidgetItem *item = new QListWidgetItem(ui->listWidget);

    //widget->setText(QString("index : %1").arg(i));

    item->setSizeHint(QSize(0, 50));

    ui->listWidget->addItem(item);
    ui->listWidget->setItemWidget(item, widget);
    //ui->listWidget->setGeometry(0, 0, 300, 350);

    widget->setChangeFun([](){
        //qDebug()<<QString("click : %1").arg(i);
    });

    widget->setDeleteFun([this,item](){
        ui->listWidget->removeItemWidget(item);
        delete item;
    });
    //widget->show();
}

void MainWindow::runActions()
{
    QRect rect = QtUtils::getInstance()->getScreenRect();

	if (m_selectWidget == nullptr)
	{
        m_selectWidget = new SelectWidget();
	}
	m_selectWidget->setSize(rect);
	m_selectWidget->show();
    this->hide();
}

void MainWindow::on_file_exit_triggered()
{
	QApplication* app;
	app->exit(0);
}

void MainWindow::on_file_import_triggered()
{
	QString filename = QFileDialog::getOpenFileName(
		this,
		"导入配置",
		QDir::currentPath(),
		"Import files (*.ini *.json);;All files(*.*)");
	if (!filename.isNull()) { //用户选择了文件
		//up_file = filename;
		//ui->lab_file->setText(up_file);
	}// else // 用户取消选择
}

void MainWindow::on_file_export_triggered()
{
	QString filename = QFileDialog::getSaveFileName(
		this,
		"导出配置",
		QDir::currentPath(),
		"Export file (*.ini *.json);;All files(*.*)");
	if (!filename.isNull()) { //用户选择了文件
							  //up_file = filename;
							  //ui->lab_file->setText(up_file);
	}// else // 用户取消选择
}
