#ifndef QTUTILS_H
#define QTUTILS_H

#include <qobject.h>

class QMouseEvent;
class QtUtils : public QObject
{
    Q_OBJECT
public:
    explicit QtUtils(QObject *parent = nullptr);
    static QtUtils* getInstance();
	//得到当前计算机的屏幕分辨率
    QRect getScreenRect();
	//获取当前鼠标所在位置
	QPoint getMousePoint();
	//移动鼠标到指定位置
	void moveMouse(QPoint point);
	//指定位置鼠标点击
	void mouseClick(QPoint point,QObject *receiver);
	////鼠标按下事件
	//void mousePressEvent(QMouseEvent *event);
	////鼠标松开事件
	//void mouseReleaseEvent(QMouseEvent *event);

signals:

public slots:

private:
    static QtUtils *m_instance;
};

#endif // QTUTILS_H
