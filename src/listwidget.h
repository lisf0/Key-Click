#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include <functional>
#include <QWidget>

class QPushButton;

namespace Ui {
class ListWidget;
}

class ListWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ListWidget(QWidget *parent = 0);
    ~ListWidget();

    void setText(const QString &text);
    void setChangeFun(std::function<void(void)> fun);
    void setDeleteFun(std::function<void(void)> fun);
private:
    void changeClick();
    void deleteClick();

private:
    Ui::ListWidget *ui;
    std::function<void(void)> m_changeFun;
    std::function<void(void)> m_deleteFun;
};

#endif // LISTWIDGET_H
