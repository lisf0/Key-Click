/********************************************************************************
** Form generated from reading UI file 'listwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LISTWIDGET_H
#define UI_LISTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ListWidget
{
public:
    QHBoxLayout *verticalLayout;
    QHBoxLayout *HLayoutLeft;
    QLabel *label;
    QHBoxLayout *HLayoutRight;
    QPushButton *btn_change;
    QPushButton *btn_delete;

    void setupUi(QWidget *ListWidget)
    {
        if (ListWidget->objectName().isEmpty())
            ListWidget->setObjectName(QStringLiteral("ListWidget"));
        ListWidget->resize(400, 50);
        verticalLayout = new QHBoxLayout(ListWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(9, 9, 9, 9);
        HLayoutLeft = new QHBoxLayout();
        HLayoutLeft->setObjectName(QStringLiteral("HLayoutLeft"));
        label = new QLabel(ListWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        HLayoutLeft->addWidget(label);


        verticalLayout->addLayout(HLayoutLeft);

        HLayoutRight = new QHBoxLayout();
        HLayoutRight->setObjectName(QStringLiteral("HLayoutRight"));
        HLayoutRight->setSizeConstraint(QLayout::SetDefaultConstraint);
        HLayoutRight->setContentsMargins(0, -1, 0, 0);
        btn_change = new QPushButton(ListWidget);
        btn_change->setObjectName(QStringLiteral("btn_change"));

        HLayoutRight->addWidget(btn_change);

        btn_delete = new QPushButton(ListWidget);
        btn_delete->setObjectName(QStringLiteral("btn_delete"));

        HLayoutRight->addWidget(btn_delete);


        verticalLayout->addLayout(HLayoutRight);


        retranslateUi(ListWidget);

        QMetaObject::connectSlotsByName(ListWidget);
    } // setupUi

    void retranslateUi(QWidget *ListWidget)
    {
        ListWidget->setWindowTitle(QApplication::translate("ListWidget", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("ListWidget", "(0.0 - 0.0)", Q_NULLPTR));
        btn_change->setText(QApplication::translate("ListWidget", "\344\277\256\346\224\271", Q_NULLPTR));
        btn_delete->setText(QApplication::translate("ListWidget", "\345\210\240\351\231\244", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ListWidget: public Ui_ListWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LISTWIDGET_H
