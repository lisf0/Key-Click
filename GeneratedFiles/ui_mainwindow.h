/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *file_import;
    QAction *file_export;
    QAction *file_exit;
    QAction *help_about;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QListWidget *listWidget;
    QHBoxLayout *HLayoutLeft;
    QPushButton *btn_addItem;
    QPushButton *btn_run;
    QMenuBar *menuBar;
    QMenu *file;
    QMenu *help;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(393, 298);
        MainWindow->setWindowOpacity(1);
        file_import = new QAction(MainWindow);
        file_import->setObjectName(QStringLiteral("file_import"));
        file_export = new QAction(MainWindow);
        file_export->setObjectName(QStringLiteral("file_export"));
        file_exit = new QAction(MainWindow);
        file_exit->setObjectName(QStringLiteral("file_exit"));
        help_about = new QAction(MainWindow);
        help_about->setObjectName(QStringLiteral("help_about"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);

        HLayoutLeft = new QHBoxLayout();
        HLayoutLeft->setObjectName(QStringLiteral("HLayoutLeft"));
        btn_addItem = new QPushButton(centralWidget);
        btn_addItem->setObjectName(QStringLiteral("btn_addItem"));

        HLayoutLeft->addWidget(btn_addItem);

        btn_run = new QPushButton(centralWidget);
        btn_run->setObjectName(QStringLiteral("btn_run"));

        HLayoutLeft->addWidget(btn_run);


        verticalLayout->addLayout(HLayoutLeft);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 393, 23));
        file = new QMenu(menuBar);
        file->setObjectName(QStringLiteral("file"));
        help = new QMenu(menuBar);
        help->setObjectName(QStringLiteral("help"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(file->menuAction());
        menuBar->addAction(help->menuAction());
        file->addAction(file_import);
        file->addAction(file_export);
        file->addAction(file_exit);
        help->addAction(help_about);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        file_import->setText(QApplication::translate("MainWindow", "\345\257\274\345\205\245", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        file_import->setShortcut(QApplication::translate("MainWindow", "Ctrl+I", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        file_export->setText(QApplication::translate("MainWindow", "\345\257\274\345\207\272", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        file_export->setShortcut(QApplication::translate("MainWindow", "Ctrl+E", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        file_exit->setText(QApplication::translate("MainWindow", "\351\200\200\345\207\272", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        file_exit->setShortcut(QApplication::translate("MainWindow", "Ctrl+F4", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        help_about->setText(QApplication::translate("MainWindow", "\345\205\263\344\272\216", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        help_about->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        btn_addItem->setText(QApplication::translate("MainWindow", "\346\267\273\345\212\240", Q_NULLPTR));
        btn_run->setText(QApplication::translate("MainWindow", "\350\277\220\350\241\214", Q_NULLPTR));
        file->setTitle(QApplication::translate("MainWindow", "\346\226\207\344\273\266", Q_NULLPTR));
        help->setTitle(QApplication::translate("MainWindow", "\345\270\256\345\212\251", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
